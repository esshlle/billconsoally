<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ page import = "com.ibm.billconsolidation.serviceimpl.MerchantServiceImpl" %>
<%@ page import = "com.ibm.billconsolidation.models.Merchant" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<group>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type='text/javascript' src='scripts.js'></script>
<link rel="stylesheet" type="text/css" href="userPage.css">
<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
<link type="text/css" rel="stylesheet" href="css/froala_blocks.css">
</group>
<title>Bill Consolidation Admin</title>
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <link type="text/css" rel="stylesheet" href="css/froala_blocks.css">
    <style>
      .fdb-block {
        border-bottom: 1px solid var(--light);
      }
    </style>
 <header class="bg-dark">
      <div class="container">
        <nav class="navbar navbar-expand-md no-gutters">
          <div class="col-3 text-left">
            <a href="https://www.froala.com">
              <img src="./imgs/logo.png" height="30" alt="image">
            </a>
          </div>
    
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse-4" aria-controls="navbarNav15" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
    
          <div class="collapse navbar-collapse navbar-collapse-4 justify-content-center col-md-6" id="navbarNav15">
            <ul class="navbar-nav justify-content-center">
              <li class="nav-item active">
                <a class="nav-link" href="adminPage.jsp">Admin Home <span class="sr-only">(current)</span></a>
              </li>
            </ul>
          </div>
    
          <div class="collapse navbar-collapse navbar-collapse-4">
            <ul class="navbar-nav ml-auto justify-content-end">
              <li class="nav-item">
                <a class="nav-link" href="billConsolidationIndex.jsp">Log Out</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </header>
  <body>
	<div>
  
    <div class="container">
    <div style="overflow-x: auto;">
    <input type="hidden" name="userId" value="${userid}">
    <strong style="color: red">${msg}</strong>

		<h1 class="sign">
			<b>Merchants</b>
		</h1>
		<table id="emp">
			<thead>
				<tr class="table">
					<th>Merchant Name</th>
					<th>Service Type</th>
					<th></th>
	
					<c:forEach items="${allMerchantList}" var="all">
						<tr>
							<td><c:out value="${all.merchantName}" /></td>
							<td><c:out value="${all.merchantType}" /></td>
							<td><a href="/BillConsolidationSystem/deleteMerchant?merchantId=<c:out value='${all.merchantId}'/>
							">Delete Merchant</a></td>
						</tr>
					</c:forEach>
				</tr>
			</thead>
		</table>
		<br>
 	    <br><button  onclick="document.getElementById('id03').style.display='block'" >Register a Merchant</button>
    </div>
             
    </div>


    </div>

    
   <div id="id03" class="modal" style=" display:none" >
  <form class="modal-content animate" action="/BillConsolidationSystem/MerchantController" method="post">
    <div class="container">
    <div style="overflow-x: auto;">
		<h1 class="sign">
			<b>Add Bill Info</b>
		</h1>
		<input type="hidden" name="userId" value="${userid}">

                    MERCHANT NAME:  <input class="un" type="text" id="merchantName" name = "merchantName" placeholder="Enter Merchant Name"  value="" required><br>
					SERVICE TYPE: <input class="un" type="text" id="serviceType" name = "serviceType" placeholder="Enter Service Type"  value="" required><br>
		
                    		<button type = "submit"  >Register</button>
                             <button type="button" onclick="document.getElementById('id03').style.display='none'">Cancel</button> 
                              
        </div>
        </div>
        </form>
        </div>  
  </body>
</html>