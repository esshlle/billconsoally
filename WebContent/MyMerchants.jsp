
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<group>
<script type='text/javascript' src='script.js'></script>
<link rel="stylesheet" type="text/css" href="userPage.css">
<link href="https://fonts.googleapis.com/css?family=Ubuntu"
	rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
<link type="text/css" rel="stylesheet" href="css/froala_blocks.css">
</group>

<header class="bg-dark">
	<div class="container">
		<nav class="navbar navbar-expand-md no-gutters">
			<div class="col-3 text-left">
				<a href="https://www.froala.com"> <img src="./imgs/logo.png"
					height="30" alt="image">
				</a>
			</div>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target=".navbar-collapse-4" aria-controls="navbarNav15"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div
				class="collapse navbar-collapse navbar-collapse-4 justify-content-center col-md-6"
				id="navbarNav15">
				<ul class="navbar-nav justify-content-center">
					<li class="nav-item active"><a class="nav-link"
						href="userPage.jsp">Home </a></li>
					<li class="nav-item"><a class="nav-link"
						href="MyMerchants.jsp">My Merchants</a></li>
					<li class="nav-item"><a class="nav-link"
						href="billHistory.jsp">Bill History <span class="sr-only">(current)</span></a></li>
				</ul>
			</div>

			<div class="collapse navbar-collapse navbar-collapse-4">
				<ul class="navbar-nav ml-auto justify-content-end">
					<li class="nav-item"><a class="nav-link"
						href="billConsolidationIndex.jsp">Log Out</a></li>
				</ul>
			</div>
		</nav>
	</div>
</header>
<body>
	<input type="hidden" name="userId" value="${userid}">


	<div id="header">
		<header>
			<p class="head" align="center">Bill Consolidation History by
				Merchant</p>
		</header>
	</div>

		<div style="overflow-x: auto;">

			<h1 class="sign">
				<b>Merchant History</b> <b> ${billNotif}</b>
			</h1>
			<form action="/BillConsolidationSystem/sortMerchant" method="get">
				 <select class="un" name="merchantName">
						<c:forEach items="${merchantList}" var="merch">
							<option value="${merch.merchantName}" > ${merch.merchantName}</option>
							</c:forEach>
							
							</select>
							<input type="hidden" value="${userid}" name="userid">
							<select class="un" name="year" id="year">
							<option value=*>Select Year</option>
							</select>
							
							<select class="un" name="month" id="month">
							<option value="">Select Month</option>
							</select>
							
							 <button type ="submit" style="margin-left: 20px;">Sort</button>
			</form>
			
			</div>
			<div>
			<div class="Con_2">
			
			<table id="emp" >
				<thead>
					<tr class="table">
						<th>Reference Number</th>
						<th>Merchant</th>
						<th>Due Date</th>
						<th>Bill Amount</th>

						<c:forEach items="${billSortMerchant}" var="billSort">
							<tr>
								<td><c:out value="${billSort.billRefNumber}" /></td>
								<td><c:out value="${billSort.merchantName}" /></td>
								<td><c:out value="${billSort.billDue}" /></td>
								<td><c:out value="${billSort.billPrice}" /></td>
							</tr>
						</c:forEach>
					</tr>
				</thead>
			</table>
				<a class= "sign"style="float: right;color: black; margin-right: 350px">BILL TOTAL: ${billSortMerchantSum}</a>		
		</div>
	</div>


</body>

<script>
for(y = 2015; y <= 2050; y++) {
    var optn = document.createElement("OPTION");
    optn.text = y;
    optn.value = y;
    
    
    document.getElementById('year').options.add(optn);
}
var d = new Date();
var monthArray = new Array();
monthArray[0] = "January";
monthArray[1] = "February";
monthArray[2] = "March";
monthArray[3] = "April";
monthArray[4] = "May";
monthArray[5] = "June";
monthArray[6] = "July";
monthArray[7] = "August";
monthArray[8] = "September";
monthArray[9] = "October";
monthArray[10] = "November";
monthArray[11] = "December";
for(m = 0; m <= 11; m++) {
    var optn = document.createElement("OPTION");
    optn.text = monthArray[m];
    // server side month start from one
    optn.value = (m+1);

    document.getElementById('month').options.add(optn);
}
</script>
