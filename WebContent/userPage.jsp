<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page
	import="com.ibm.billconsolidation.serviceimpl.MerchantServiceImpl"%>
<%@ page import="com.ibm.billconsolidation.models.Merchant"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<title>Home</title>
<group>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<script type='text/javascript' src='scripts.js'></script>
<link rel="stylesheet" type="text/css" href="userPage.css">
<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
<link type="text/css" rel="stylesheet" href="css/froala_blocks.css">
</group>

<header class="bg-dark">
	<div class="container">
		<nav class="navbar navbar-expand-md no-gutters">
			<div class="col-3 text-left">
				<a href="https://www.froala.com"> <img src="./imgs/logo.png"
					height="30" alt="image">
				</a>
			</div>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target=".navbar-collapse-4" aria-controls="navbarNav15"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div
				class="collapse navbar-collapse navbar-collapse-4 justify-content-center col-md-6"
				id="navbarNav15">
				<ul class="navbar-nav justify-content-center">
					<li class="nav-item active"><a class="nav-link" href="userPage.jsp">Home
							<span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item"><a class="nav-link"
						href="MyMerchants.jsp">My Merchants</a></li>
					<li class="nav-item"><a class="nav-link"
						href="billHistory.jsp?">Bill History</a></li>
				</ul>
			</div>

			<div class="collapse navbar-collapse navbar-collapse-4">
				<ul class="navbar-nav ml-auto justify-content-end">
					<li class="nav-item"><a class="nav-link"
						href="billConsolidationIndex.jsp">Log Out</a></li>
				</ul>
			</div>
		</nav>
	</div>
</header>

<body>

	<div class="Con_2">
		<strong style="color: red">${msg}</strong>

		<div style="overflow-x: auto;">

			<h1 class="sign">
				<b>Total Bill for the Month</b> <b> ${billNotif}</b>
			</h1>
			<table id="emp">
				<thead>
					<tr class="table">
						<th>Reference Number</th>
						<th>Merchant</th>
						<th>Due Date</th>
						<th>Bill Amount</th>
						<th></th>

						<c:forEach items="${billList}" var="bill">
							<tr>
								<td><c:out value="${bill.billRefNumber}" /></td>
								<td><c:out value="${bill.merchantName}" /></td>
								<td><c:out value="${bill.billDue}" /></td>
								<td><c:out value="${bill.billPrice}" /></td>
								<td>
								<td><a
									href="/BillConsolidationSystem/deleteBill?billRefNumber=<c:out value='${bill.billRefNumber}'/>&userId=<c:out value='${userid}'/>">Remove</a></td>
							</tr>
						</c:forEach>
					</tr>
				</thead>
			</table>
			<a class="sign"
				style="float: right; color: black; margin-right: 350px">BILL
				TOTAL: ${billSum}</a> <br>
			<button
				onclick="document.getElementById('id02').style.display='block'">Add
				a Bill</button>

		</div>

	</div>
	<div style="overflow-x: auto;" class=Con_2>

		<h1 class="sign">
			<b>My Merchants</b>
		</h1>
		<table id="emp">
			<thead>
				<tr class="table">
					<th>Merchant Name</th>
					<th>Service Type</th>
					<th></th>
					<c:forEach items="${merchantList}" var="merchant">
						<tr>
							<td><c:out value="${merchant.merchantName}" /></td>
							<td><c:out value="${merchant.merchantType}" /></td>
							<td><a
								href="/BillConsolidationSystem/removeMerchant?merchantId=<c:out value='${merchant.merchantId}'/>&userId=<c:out value='${userid}'/>">Remove</a></td>
						</tr>
					</c:forEach>
				</tr>
			</thead>
		</table>
		<br>
		<button
			onclick="document.getElementById('id01').style.display='block'">Add
			a Merchant</button>
	</div>




	<div id="id01" class="modal" style="display: none">
		<%
			HttpSession modalTable = request.getSession();
			MerchantServiceImpl allMerchant = new MerchantServiceImpl();
			List<Merchant> allMerchantList = allMerchant.allMerchant();
			modalTable.setAttribute("allMerchantList", allMerchantList);
		%>

		<form class="modal-content animate" action="addUserMerchant"
			method="get">
			<input type="hidden" name="userId" value="${userid}">
			<div class="container">
				<div style="overflow-x: auto;">

					<h1 class="sign">
						<b>Merchants</b>
					</h1>
					<table id="emp">


						<thead>
							<tr class="table">
								<th>Merchant Name</th>
								<th>Service Type</th>
								<th></th>

								<c:forEach items="${allMerchantList}" var="all">
									<tr>
										<td><c:out value="${all.merchantName}" /></td>
										<td><c:out value="${all.merchantType}" /></td>
										<td><a
											href="/BillConsolidationSystem/addUserMerchant?merchantId=<c:out value='${all.merchantId}'/>&userId=<c:out value='${userid}'/>">Add
												as my Merchant</a></td>
									</tr>
								</c:forEach>
							</tr>
						</thead>
					</table>
					<br>
					<button type="button" onclick="closeForm()">Cancel</button>
				</div>
			</div>
		</form>
	</div>
	<div id="id02" class="modal" style="display: none">
		<form class="modal-content animate"
			action="/BillConsolidationSystem/BillController" method="post">
			<div class="container">
				<div style="overflow-x: auto;">
					<h1 class="sign">
						<b>Add Bill Info</b>
					</h1>
					<input type="hidden" name="userId" value="${userid}"> BILL
					REFERENCE NUMBER: <input class="un" type="text" id="billRef"
						name="billRef" placeholder="Enter reference number" value=""
						required><br>
						 MERCHANT: <select class="un" name="merchantName">
						<c:forEach items="${merchantList}" var="merch">
							<option value="${merch.merchantName}">
								${merch.merchantName}</option>
						</c:forEach>
					</select>
					<br> 
					BILL AMOUNT: <input class="un" type="number" step=".01" id="billAmount" name="billAmount" placeholder="Enter bill amount" value="" required><br> 
					BILLING DATE: <input class="un" type="text" name="billDate" id="datepicker"> <br>
					DUE DATE: <input class="un" type="text" name="billDue" id="dateDue">
					<br>
					<button type="submit">Add Bill</button>
					<button type="button"
						onclick="document.getElementById('id02').style.display='none'">Cancel</button>

				</div>
			</div>
		</form>
	</div>
</body>

<script>
		$(function() {
			$("#datepicker").datepicker();
			$("#datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
		});
		$(function() {
			$("#dateDue").datepicker();
			$("#dateDue").datepicker("option", "dateFormat", "yy-mm-dd");
		});
</script> 

</html>