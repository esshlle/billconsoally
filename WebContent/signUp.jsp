<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<meta charset="ISO-8859-1">
<meta charset="utf-8">
<meta charset="ISO-8859-1">
    
<title>Sign Up</title>
</head>
<body>

 <header class="bg-dark">
  <div class="container">
        <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="https://www.froala.com">
            <img src="./imgs/logo.png" height="30" alt="image">
          </a>
    
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav13" aria-controls="navbarNav13" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
    
          <div class="collapse navbar-collapse" id="navbarNav13">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a  href="billConsolidationIndex.jsp" style="margin-left: 900px;">Log In</a>
              </li>
            </ul>
    
          </div>
        </nav>
      </div>
      </header>
	<div id="login">
		<div class="container">
			<div id="login-row"
				class="row justify-content-center align-items-center">
				<form id="sign-up" class="form" action="SignUpController" method="post"> <br><br>
					<h3 class="text-center text-info">Sign Up</h3>
					<div class="form-group">
						<label for="use" class="text-info">First Name:</label><br> <input
							type="text" name="firstName" id="firstName" class="form-control" required>
					</div>
					<div class="form-group">
						<label for="use" class="text-info">Last Name:</label><br> <input
							type="text" name="lastName" id="lastName" class="form-control" required >
					</div>

					<div class="form-group">
						<label for="user" class="text-info">Email:</label><br> <input
							type="email" name="email" id="email" class="form-control">
					</div>

					<div class="form-group">
						<label for="user" class="text-info">Username:</label><br> <input
							type="text" name="userName" id="userName" class="form-control" required>
					</div>

					<div class="form-group">
						<label for="password" class="text-info">Password:</label><br>
						<input type="password" name="password" id="password" class="form-control" required>
					</div>

					<div class="form-check">
						<label class="form-check-label"> <input type="checkbox"
							class="form-check-input" required> I Read and Accept Terms and Conditions
						</label> <br> <br>
					</div>
					<div class="form-group">
						<input type="submit" name="submit" class="btn btn-info btn-md"
							value="Sign Up">

						<a  id="cancel" class="btn btn-info btn-md" href = "billConsolidationIndex.jsp">
							Cancel</a>
					</div>
						<p style="color: red">${message0}</p>
						</form>
			</div>
			</div>
		

		</div>
</body>



</html>