<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<!DOCTYPE html>
<html>
<head>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<meta charset="ISO-8859-1">
<meta charset="utf-8">
<title>Bill Consolidation</title>
<b>${message0}</b>
</head>
<header class="bg-dark">
  <div class="container">
        <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="https://www.froala.com">
            <img src="./imgs/logo.png" height="30" alt="image">
          </a>
    
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav13" aria-controls="navbarNav13" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
    
      </header>

 
<body>
    <div id="login">
        <h3 class="text-center text-white pt-5">Login form</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="LoginController" method="post">
                            <h3 class="text-center text-info">Login</h3>
                            <div class="form-group">
                                <label for="user" class="text-info">Username:</label><br>
                                <input type="text" name="user" id="user" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                               ${message}
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="Log In">
                                 <br><br>
                                 Have no account yet? <a class="text-info" href="signUp.jsp">Sign Up</a> now!
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</body>

</html>