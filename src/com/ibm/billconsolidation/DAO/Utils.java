package com.ibm.billconsolidation.DAO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Utils {

	public Utils() {
		super();
		// TODO Auto-generated constructor stub
	}
	

 public String[] getDateNow() {
	 String dateNow[] = new String[2];
	 Date date = Calendar.getInstance().getTime();
     DateFormat year = new SimpleDateFormat("yyyy");  
     DateFormat mon = new SimpleDateFormat("MM"); 
     dateNow[0]= year.format(date);
     dateNow[1]  = mon.format(date);
     return dateNow;
 }
 
 public boolean checkIfNull(String...strings) {
	 boolean notNull = false;
	 
	 for(String strTemp : strings) {
		 if (strTemp == null) {
			 notNull = false;
		 }
		 if (strTemp.isEmpty()) {
			 notNull = false;
		 }
		 if (strTemp.length()==0) {
			 notNull = false;
		 }
	 }
	 
	return notNull; 
 }
 
}