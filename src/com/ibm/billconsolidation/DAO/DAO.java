package com.ibm.billconsolidation.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DAO {
	private Connection jdbcConnection;

	public DAO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void dbConnect() {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		}

		try {
			setJdbcConnection(DriverManager
					.getConnection("jdbc:mariadb://localhost:3307/casestudy?user=root&password=root"));

		} catch (SQLException ex) {

			System.out.println("Problem connecting to the database." + ex);
		}

	}

	public void dbDisconnect() {
		try {
			getJdbcConnection().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public Connection getJdbcConnection() {
		return jdbcConnection;
	}

	public void setJdbcConnection(Connection jdbcConnection) {
		this.jdbcConnection = jdbcConnection;
	}
}
