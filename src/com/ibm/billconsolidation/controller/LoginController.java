package com.ibm.billconsolidation.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ibm.billconsolidation.DAO.Utils;
import com.ibm.billconsolidation.models.Bill;
import com.ibm.billconsolidation.models.Merchant;
import com.ibm.billconsolidation.serviceimpl.BillServiceImpl;
import com.ibm.billconsolidation.serviceimpl.LoginAuthServiceImpl;
import com.ibm.billconsolidation.serviceimpl.MerchantServiceImpl;

@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginController() {
		super();
	
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String user = request.getParameter("user");
		String password = request.getParameter("password");
		LoginAuthServiceImpl userLog = new LoginAuthServiceImpl(user, password);
		String logIn[] = userLog.LoginControl();
		HttpSession session = request.getSession();
		
		Utils util = new Utils();
		String dateNow[] = util.getDateNow();
		

		String destPage = "billConsolidationIndex.jsp";
		if (logIn[0].equalsIgnoreCase("admin")) {
			HttpSession modalTable = request.getSession();
			MerchantServiceImpl allMerchant = new MerchantServiceImpl();
			List<Merchant> allMerchantList = allMerchant.allMerchant();
			modalTable.setAttribute("allMerchantList", allMerchantList);
			
			destPage = "adminPage.jsp";
		}
		if (logIn[0].equalsIgnoreCase("user")) {
			destPage = "userPage.jsp";
			MerchantServiceImpl merchant = new MerchantServiceImpl();
			List<Merchant> merchantList = merchant.userMerchant(logIn[1]);
			session.setAttribute("merchantList", merchantList);
			BillServiceImpl bill = new BillServiceImpl();
			List<Bill> billList = bill.retrieveBillbyDate(logIn[1], dateNow[0], dateNow[1]);
			String billSum = bill.computeBill(logIn[1], dateNow[0], dateNow[1]);
			session.setAttribute("billSum", billSum);
			session.setAttribute("billList", billList);
			
		}

		if (logIn[0].equalsIgnoreCase("error")) {
			String message = "Invalid username/password";
			session.setAttribute("message", message);

		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(destPage);
		session.setAttribute("userid", logIn[1]);
		dispatcher.forward(request, response);

		doGet(request, response);
	
	}
	
	
}
