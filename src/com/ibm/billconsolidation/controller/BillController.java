package com.ibm.billconsolidation.controller;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ibm.billconsolidation.DAO.Utils;
import com.ibm.billconsolidation.models.Bill;
import com.ibm.billconsolidation.models.Merchant;
import com.ibm.billconsolidation.serviceimpl.BillServiceImpl;
import com.ibm.billconsolidation.serviceimpl.MerchantServiceImpl;


@WebServlet(urlPatterns = {"/BillController","/sortBill","/deleteBill"})
public class BillController extends HttpServlet implements Serializable {
	private static final long serialVersionUID = 1L;
    public BillController() {
        super();
 
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getServletPath();
		  try {
	            switch (action) {
	            case "/sortBill":
	                sortBill(request, response);
	                break;
	            case "/deleteBill": {
	            	deleteBill(request,response);
	            }
	            }
	        } catch (SQLException ex) {
	            throw new ServletException(ex);
	        }
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId = request.getParameter("userId");
		String billRef = request.getParameter("billRef");
		String merchantName = request.getParameter("merchantName");
		String billAmount = request.getParameter("billAmount");
		String billDate = request.getParameter("billDate");
		String billDue= request.getParameter("billDue");
		 String msg= null;

		//add bill
		Bill newBillInfo = new Bill(userId,billRef,merchantName,billAmount,billDate,billDue);
		BillServiceImpl addBill = new BillServiceImpl(); 
		String add = addBill.addBill(newBillInfo);
		
		if (add.equalsIgnoreCase("OK")) {
			msg="You have successfully added a Bill";

        }
        if (add.equalsIgnoreCase("Duplicate")) {
        	msg="ERROR: Bill Reference Number is already existing, try again.";


	}
        request.setAttribute("msg",msg);
		returnToHome(request, response);
	}
	
	 public void deleteBill(HttpServletRequest request, HttpServletResponse response) 
	    		throws ServletException, IOException, SQLException {
	    	String billRefNumber = request.getParameter("billRefNumber");
			//instantiate implementation for removing Bill
			BillServiceImpl bill = new BillServiceImpl();
			bill.removeBill(billRefNumber);
			returnToHome(request,response);
	        
		
	    }
	 
	 
	 public void sortBill(HttpServletRequest request, HttpServletResponse response) 
	    		throws ServletException, IOException{
		 String userId = request.getParameter("userid");   
	    	String dueYY = request.getParameter("year");
	    	String dueMM = request.getParameter("month");
	        System.out.println(dueYY+dueMM);
	        
			//instantiate implementation for viewing all merchant
	        BillServiceImpl billSort = new BillServiceImpl();
	        List<Bill> billSorted = billSort.retrieveBillbyDate(userId, dueYY, dueMM);
	        String billSortSum = billSort.computeBill(userId, dueYY, dueMM);
	        System.out.println(userId);
	        
	        // session
	        HttpSession session = request.getSession();
	 		session.setAttribute("billSorted",billSorted);
	 		session.setAttribute("billSortSum",billSortSum);
	 		RequestDispatcher dispatcher = request.getRequestDispatcher("billHistory.jsp");
	 		dispatcher.forward(request, response);
		 
	 }
	 
	 public void returnToBill(HttpServletRequest request, HttpServletResponse response) 
	    		throws ServletException, IOException{
	    	//return values to page
			String userId = request.getParameter("userid");
			Utils util = new Utils();
			String dateNow[] = util.getDateNow();
			System.out.println(util.checkIfNull(userId));
			
			//return values to page
			MerchantServiceImpl merchant = new MerchantServiceImpl();
			List<Merchant> merchantList = merchant.userMerchant(userId);
			BillServiceImpl bill = new BillServiceImpl();
			String billSum = bill.computeBill(userId, dateNow[0], dateNow[1]);
			List<Bill> billList = bill.retrieveBillbyDate(userId, dateNow[0], dateNow[1]);

	 		//insert to session
	 	    HttpSession session = request.getSession();
	 		session.setAttribute("merchantList", merchantList);
	 		session.setAttribute("billList", billList);
	 		session.setAttribute("billSum", billSum);
	 		

	    }
	 
	 public void returnToHome(HttpServletRequest request, HttpServletResponse response) 
	    		throws ServletException, IOException{
	    	//return values to page
			String userId = request.getParameter("userId");
			Utils util = new Utils();
			String dateNow[] = util.getDateNow();
			System.out.println(util.checkIfNull(userId));
			
			//return values to page
			MerchantServiceImpl merchant = new MerchantServiceImpl();
			List<Merchant> merchantList = merchant.userMerchant(userId);
			BillServiceImpl bill = new BillServiceImpl();
			String billSum = bill.computeBill(userId, dateNow[0], dateNow[1]);
			List<Bill> billList = bill.retrieveBillbyDate(userId, dateNow[0], dateNow[1]);

	 		//insert to session
	 	    HttpSession session = request.getSession();
	 		session.setAttribute("merchantList", merchantList);
	 		session.setAttribute("billList", billList);
	 		session.setAttribute("billSum", billSum);
	 		RequestDispatcher dispatcher = request.getRequestDispatcher("userPage.jsp");
	 		dispatcher.forward(request, response);	
	 		
	    }
	 
	


}
