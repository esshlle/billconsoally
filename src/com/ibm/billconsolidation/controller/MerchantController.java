package com.ibm.billconsolidation.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.ibm.billconsolidation.DAO.Utils;
import com.ibm.billconsolidation.models.Bill;
import com.ibm.billconsolidation.models.Merchant;
import com.ibm.billconsolidation.serviceimpl.BillServiceImpl;
import com.ibm.billconsolidation.serviceimpl.MerchantServiceImpl;

@WebServlet(urlPatterns = { "/MerchantController", "/removeMerchant", "/addUserMerchant", "/sortMerchant",
		"/deleteMerchant" })
public class MerchantController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MerchantController() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getServletPath();

		try {
			switch (action) {
			case "/removeMerchant":
				removeMerchant(request, response);
				break;
			case "/sortMerchant":
				sortByMerchant(request, response);
				break;
			case "/deleteMerchant":
				deleteMerchant(request, response);
				break;
			case "/addUserMerchant":
				addUserMerchant(request, response);
				break;

			}
		} catch (SQLException ex) {
			throw new ServletException(ex);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String merchantName = request.getParameter("merchantName");
		String serviceType = request.getParameter("serviceType");

		// register merchant
		Merchant merchant = new Merchant(merchantName, serviceType);
		MerchantServiceImpl merchantService = new MerchantServiceImpl();
		String add = merchantService.addMerchant(merchant);
		String msg = null;

		if (add.equalsIgnoreCase("OK")) {
			msg = "You have successfully added a Merchant";

		}
		if (add.equalsIgnoreCase("Duplicate")) {
			msg = "Merchant is already added, try again.";
		}

		// push to session
		HttpSession session = request.getSession();
		request.setAttribute("msg", msg);
		List<Merchant> allMerchantList = merchantService.allMerchant();
		session.setAttribute("allMerchantList", allMerchantList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("adminPage.jsp");
		dispatcher.forward(request, response);

	}

	private void removeMerchant(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {

		// define parameters
		String userId = request.getParameter("userId");
		String merchantId = request.getParameter("merchantId");

		// instantiate implementation for remove merchant
		MerchantServiceImpl merchant = new MerchantServiceImpl();
		merchant.removeUserMerchant(userId, merchantId);
		returnToHome(request, response);

	}

	private void deleteMerchant(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {

		String merchantId = request.getParameter("merchantId");

		// instantiate implementation for remove merchant
		MerchantServiceImpl merchant = new MerchantServiceImpl();
		merchant.deleteMerchant(merchantId);

		HttpSession modalTable = request.getSession();
		MerchantServiceImpl allMerchant = new MerchantServiceImpl();
		List<Merchant> allMerchantList = allMerchant.allMerchant();
		modalTable.setAttribute("allMerchantList", allMerchantList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("adminPage.jsp");
		dispatcher.forward(request, response);

	}

	public void addUserMerchant(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		String userId = request.getParameter("userId");
		String merchantId = request.getParameter("merchantId");
		String msg = null;

		// instantiate implementation for viewing all merchant
		MerchantServiceImpl merchant = new MerchantServiceImpl();
		String add = merchant.addUserMerchant(userId, merchantId);

		if (add.equalsIgnoreCase("OK")) {
			msg = "You have successfully added a Merchant";

		}
		if (add.equalsIgnoreCase("Duplicate")) {
			msg = "Merchant is already added, try again.";

		}
		request.setAttribute("msg", msg);
		returnToHome(request, response);

	}

	public void sortByMerchant(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		String userId = request.getParameter("userid");
		String dueYY = request.getParameter("year");
		String dueMM = request.getParameter("month");
		String merchantId = request.getParameter("merchantName");
		System.out.println(merchantId);

		System.out.println(dueYY + dueMM);
		// instantiate implementation for viewing all merchant
		BillServiceImpl billSort = new BillServiceImpl();
		List<Bill> billSortedMerchant = billSort.retrieveBillByMerchant(userId, merchantId, dueYY, dueMM);
		String billSortSumMerchant = billSort.computeBillMerchant(userId,merchantId, dueYY, dueMM);
		System.out.println(userId);

		// insert msg to session
		HttpSession session = request.getSession();
		session.setAttribute("billSortMerchant", billSortedMerchant);
		session.setAttribute("billSortMerchantSum", billSortSumMerchant);
		RequestDispatcher dispatcher = request.getRequestDispatcher("MyMerchants.jsp");
		dispatcher.forward(request, response);

	}

	public void returnToHome(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		// return values to page
		String userId = request.getParameter("userId");
		Utils util = new Utils();
		String dateNow[] = util.getDateNow();
		System.out.println(util.checkIfNull(userId));

		// return values to page
		MerchantServiceImpl merchant = new MerchantServiceImpl();
		List<Merchant> merchantList = merchant.userMerchant(userId);
		BillServiceImpl bill = new BillServiceImpl();
		String billSum = bill.computeBill(userId, dateNow[0], dateNow[1]);
		List<Bill> billList = bill.retrieveBillbyDate(userId, dateNow[0], dateNow[1]);

		// insert to session
		HttpSession session = request.getSession();
		session.setAttribute("merchantList", merchantList);
		session.setAttribute("billList", billList);
		session.setAttribute("billSum", billSum);

		RequestDispatcher dispatcher = request.getRequestDispatcher("userPage.jsp");
		dispatcher.forward(request, response);

	}

}
