package com.ibm.billconsolidation.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.billconsolidation.models.User;
import com.ibm.billconsolidation.serviceimpl.UserServiceImpl;;

@WebServlet("/SignUpController")
public class SignUpController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public SignUpController() {
        super();

    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String userName = request.getParameter("userName");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		User newUser = new User(firstName,lastName,email,userName,password);
		UserServiceImpl notice = new UserServiceImpl();
		
		String destPage = "signUp.jsp";
		String signUpNotice = notice.signUpUser(newUser);
		String message0 = null;
		
		if (signUpNotice.equalsIgnoreCase("Sign Up Success")) {
			destPage = "billConsolidationIndex.jsp";
			message0 = "Sign up success! Please log in";
	
		}
		if (signUpNotice.equalsIgnoreCase("Duplicate")) {
			message0 = "Error signing up, you must have entered an already used email or password";
	
		}
		
		request.setAttribute("message0", message0);
		RequestDispatcher dispatcher = request.getRequestDispatcher(destPage);
		dispatcher.forward(request, response);
		
		

		
		
		doGet(request, response);
	}

}
