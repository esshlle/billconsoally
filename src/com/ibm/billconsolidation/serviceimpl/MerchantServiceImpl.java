package com.ibm.billconsolidation.serviceimpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import com.ibm.billconsolidation.DAO.DAO;
import com.ibm.billconsolidation.models.Merchant;
import com.ibm.billconsolidation.service.MerchantService;

public class MerchantServiceImpl implements MerchantService {

	
	@Override
	public List<Merchant> userMerchant(String userId) {
		List<Merchant> userMerchant = new ArrayList<>();
		DAO userDAO = new DAO();
		userDAO.dbConnect();
		String merchantList = "SELECT USER_MERCHANTS.MERCHANT_ID,"
				+ "MERCHANT.MERCHANT_ID,"
				+ "MERCHANT_NAME, "
				+ "SERVICE_TYPE FROM MERCHANT, "
				+ "USER_MERCHANTS WHERE MERCHANT.MERCHANT_ID = USER_MERCHANTS.MERCHANT_ID "
				+ "AND USER_MERCHANTS.USER_ID = ? "
				+ "ORDER BY MERCHANT_NAME";
		PreparedStatement merchantListStmt;
		try {
			merchantListStmt = userDAO.getJdbcConnection().prepareStatement(merchantList);
			merchantListStmt.setString(1, userId);
			ResultSet rs = merchantListStmt.executeQuery();
			
			while (rs.next()) {
				String merchantId = rs.getString("MERCHANT_ID");
				String merchantName = rs.getString("MERCHANT_NAME");
				String serviceType = rs.getString("SERVICE_TYPE");
				Merchant merchant = new Merchant(merchantId,merchantName,serviceType);
				userMerchant.add(merchant);
				System.out.println(merchant);
				
			}

			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
        userDAO.dbDisconnect();
		return userMerchant;

		
	}

	@Override
	public boolean removeUserMerchant(String userId, String merchantId) {
		boolean rowDeleted = false;
		DAO userDAO  = new DAO();
		userDAO.dbConnect();
		String removeMerchant = "DELETE FROM user_merchants WHERE USER_ID=? AND MERCHANT_ID = ?";
		PreparedStatement removeMerchantStmt;
			try {
				removeMerchantStmt = userDAO.getJdbcConnection().prepareStatement(removeMerchant);
				removeMerchantStmt.setString(1, userId);
				removeMerchantStmt.setString(2, merchantId);
				rowDeleted = removeMerchantStmt.executeUpdate() > 0;
				removeMerchantStmt.close();
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
      
        userDAO.dbDisconnect();
		return rowDeleted;
	}

	@Override
	public List<Merchant> allMerchant() {
		List<Merchant> allMerchant = new ArrayList<>();
		DAO userDAO = new DAO();
		userDAO.dbConnect();
		String qry = "SELECT * FROM MERCHANT ORDER BY MERCHANT_NAME";
		
		PreparedStatement merchantListStmt;
		try {
			merchantListStmt = userDAO.getJdbcConnection().prepareStatement(qry);
			ResultSet rs = merchantListStmt.executeQuery();
			
			while (rs.next()) {
				String merchantId = rs.getString("MERCHANT_ID");
				String merchantName = rs.getString("MERCHANT_NAME");
				String serviceType = rs.getString("SERVICE_TYPE");
				Merchant merchant = new Merchant(merchantId,merchantName,serviceType);
				allMerchant.add(merchant);
				
			}

			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allMerchant;
	}


	@Override
	public String addUserMerchant(String userId, String merchantId) {
		String addUserMerchant = null;
		DAO userDAO  = new DAO();
		userDAO.dbConnect();
		String qry = "INSERT INTO USER_MERCHANTS VALUES(?,?)";
		PreparedStatement addUserMerchantStmt;
			try {
				addUserMerchantStmt = userDAO.getJdbcConnection().prepareStatement(qry);
				addUserMerchantStmt.setString(1, userId);
				addUserMerchantStmt.setString(2, merchantId);
				int i =addUserMerchantStmt.executeUpdate();
				
				while (i>0) {
					addUserMerchant = "OK";
					break;
				}
				
				
			} catch (Exception e) {
				if (e instanceof SQLIntegrityConstraintViolationException) {
					addUserMerchant = "duplicate";
					System.out.println(addUserMerchant);
				}
				System.out.println(e);
			}
			
      
        userDAO.dbDisconnect();
		return addUserMerchant;
	}

	@Override
	public String addMerchant(Merchant newMerchant) {
		String registerMerchantStatus = null;

		String merchantName = newMerchant.getMerchantName();
		String serviceType =  newMerchant.getMerchantType();

		DAO userDAO = new DAO();
		userDAO.dbConnect();
		String qry = "INSERT INTO MERCHANT VALUES(0,?,?)";
		PreparedStatement addMerchantStmt;
		try {
			addMerchantStmt = userDAO.getJdbcConnection().prepareStatement(qry);
			addMerchantStmt.setString(1, merchantName);
			addMerchantStmt.setString(2, serviceType);
		
			int i = addMerchantStmt.executeUpdate();

			if (i > 0) {
				registerMerchantStatus = "OK";
				System.out.println(registerMerchantStatus);

			}
		} catch (Exception e) {
			if (e instanceof SQLIntegrityConstraintViolationException) {
				registerMerchantStatus = "duplicate";
				System.out.println(e);
			}

		}

		userDAO.dbDisconnect();
		return registerMerchantStatus;
	}

	@Override
	public boolean deleteMerchant(String merchantId) {
		boolean rowDeleted = false;
		DAO userDAO  = new DAO();
		userDAO.dbConnect();
		String qry = "DELETE FROM MERCHANT WHERE MERCHANT_ID = ?";
		PreparedStatement deleteMerchantStmt;
			try {
				deleteMerchantStmt = userDAO.getJdbcConnection().prepareStatement(qry);
				deleteMerchantStmt.setString(1, merchantId);
				rowDeleted = deleteMerchantStmt.executeUpdate() > 0;
				deleteMerchantStmt.close();
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			
		
        userDAO.dbDisconnect();
		return rowDeleted;
	}

}
