package com.ibm.billconsolidation.serviceimpl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import com.ibm.billconsolidation.DAO.DAO;
import com.ibm.billconsolidation.service.LoginAuthService;

public class LoginAuthServiceImpl implements LoginAuthService {
	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LoginAuthServiceImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoginAuthServiceImpl(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	@Override
	public String[] LoginControl() {
		String adminLevel[] = new String[2];
		DAO loginDAO = new DAO();
		loginDAO.dbConnect();
			String getMatch = "SELECT * FROM USERS WHERE USER_NAME=? AND PASSWORD=?";
			PreparedStatement stmt;
			try {
				stmt = loginDAO.getJdbcConnection().prepareStatement(getMatch);
				stmt.setString(1, username);
				stmt.setString(2, password);
				ResultSet rs = stmt.executeQuery();
				if (rs.next()) {
					adminLevel[1] = rs.getString("USER_ID");
				
					if(rs.getString("USER_LEVEL").equalsIgnoreCase("admin")) {
						adminLevel[0] = "admin";
					}
					if(rs.getString("USER_LEVEL").equalsIgnoreCase("user")) {
						adminLevel[0] = "user";
					}}
					else { adminLevel[0] = "error";
				}
	   
		
				loginDAO.getJdbcConnection().setAutoCommit(false);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	
		return adminLevel;
	}



}
