package com.ibm.billconsolidation.serviceimpl;

import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;

import com.ibm.billconsolidation.DAO.DAO;
import com.ibm.billconsolidation.models.User;
import com.ibm.billconsolidation.service.UserService;

public class UserServiceImpl implements UserService {

	@Override
	public String signUpUser(User newUser){
		String signUpStatus = null;
		String first_name = newUser.getFirstName();
		String last_name = newUser.getLastName();
		String email = newUser.getEmail();
		String user_name = newUser.getUserName();
		String password = newUser.getPassword();
		
		DAO userDAO = new DAO();
		userDAO.dbConnect();
		String addUser = "INSERT INTO USERS VALUES(0,'USER',?,?,?,?,?)";
		PreparedStatement addUserstmt;
		try {
			addUserstmt = userDAO.getJdbcConnection().prepareStatement(addUser);
			addUserstmt.setString(1, first_name);
			addUserstmt.setString(2, last_name);
			addUserstmt.setString(3, email);
			addUserstmt.setString(4, user_name);
			addUserstmt.setString(5, password);
			int i = addUserstmt.executeUpdate();

			if (i > 0) {
				signUpStatus = "Sign up success";
			}
		} catch (Exception e) {
			if (e instanceof SQLIntegrityConstraintViolationException) {
				signUpStatus = "Duplicate";
				System.out.println(signUpStatus);
		}
		
		}
		return signUpStatus;
	}


}
