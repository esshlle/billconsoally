package com.ibm.billconsolidation.serviceimpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import com.ibm.billconsolidation.DAO.DAO;
import com.ibm.billconsolidation.models.Bill;
import com.ibm.billconsolidation.service.BillService;

public class BillServiceImpl implements BillService {

	@Override
	public List<Bill> retrieveAllUserBills(String userId) {
		List<Bill> userBill = new ArrayList<>();
		DAO userDAO = new DAO();
		userDAO.dbConnect();
		String retrieveBill = "SELECT * FROM BILL WHERE USER_ID=?";
		PreparedStatement retrieveBillStmt;
		try {
			retrieveBillStmt = userDAO.getJdbcConnection().prepareStatement(retrieveBill);
			retrieveBillStmt.setString(1, userId);
			ResultSet rs = retrieveBillStmt.executeQuery();

			while (rs.next()) {
				String id = rs.getString("BILL_ID");
				int billId = Integer.parseInt(id);
				String refNum = rs.getString("REF_NUMBER");
				String merchantName = rs.getString("MERCHANT_NAME");
				String billAmount = rs.getString("AMOUNT");
				String billDue = rs.getString("DUE_DATE");
				Bill billList = new Bill(billId,refNum, merchantName, billAmount, billDue);
				userBill.add(billList);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			userDAO.dbDisconnect();
		}


		return userBill;

	}

	@Override
	public String addBill(Bill newBillInfo) {
		String billStatus = null;

		String userId = newBillInfo.getUserId();
		String billRef = newBillInfo.getBillRefNumber();
		String merchantName = newBillInfo.getMerchantName();
		String billAmount = newBillInfo.getBillPrice();
		String billDateIssue = newBillInfo.getBillDate();
		String billDueDate = newBillInfo.getBillDue();

		DAO userDAO = new DAO();
		userDAO.dbConnect();
		String addBill = "INSERT INTO BILL VALUES(0,?,?,?,?,?,?)";
		PreparedStatement addBillstmt;
		try {
			addBillstmt = userDAO.getJdbcConnection().prepareStatement(addBill);
			addBillstmt.setString(1, userId);
			addBillstmt.setString(2, billRef);
			addBillstmt.setString(3, merchantName);
			addBillstmt.setString(4, billAmount);
			addBillstmt.setString(5, billDateIssue);
			addBillstmt.setString(6, billDueDate);

			int i = addBillstmt.executeUpdate();

			if (i > 0) {
				billStatus = "OK";
				System.out.println(billStatus);

			}
		} catch (Exception e) {
			if (e instanceof SQLIntegrityConstraintViolationException) {
				billStatus = "duplicate";
				System.out.println(e);
			}
		

		}
		finally {
			userDAO.dbDisconnect();
		}

		
		return billStatus;
	}

	@Override
	public boolean removeBill(String billRefNumber) {
		boolean rowDeleted = false;
		DAO userDAO  = new DAO();
		userDAO.dbConnect();
		String qry = "DELETE FROM BILL WHERE REF_NUMBER=?";
		PreparedStatement removeBillStmt;
			try {
				removeBillStmt = userDAO.getJdbcConnection().prepareStatement(qry);
				removeBillStmt.setString(1, billRefNumber);
				rowDeleted = removeBillStmt.executeUpdate() > 0;
				removeBillStmt.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
      
			finally {
				userDAO.dbDisconnect();
			}

		return rowDeleted;
	}


	@Override
	public String computeBill(String userId, String dueYY, String dueMM) {
		String sum = null;
		DAO userDAO = new DAO();
		userDAO.dbConnect();
		String qry = "SELECT SUM(AMOUNT) FROM "
				+ "BILL WHERE USER_ID=? AND YEAR(DUE_DATE)=? "
				+ "AND MONTH(DUE_DATE)=? "
				+ "AND AMOUNT>0";
	
		PreparedStatement sumStmt;
		try {
			sumStmt = userDAO.getJdbcConnection().prepareStatement(qry);
			sumStmt.setString(1, userId);
			sumStmt.setString(2, dueYY);
			sumStmt.setString(3, dueMM);
			ResultSet rs = sumStmt.executeQuery();
			while (rs.next()) {
			sum = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			userDAO.dbDisconnect();
		}

		return sum;

	}
	
	public String computeBillMerchant(String userId, String merchant, String dueYY, String dueMM) {
		String sum = null;
		DAO userDAO = new DAO();
		userDAO.dbConnect();
		String qry = "SELECT SUM(AMOUNT) FROM "
				+ "BILL WHERE USER_ID=? "
				+ "AND MERCHANT_NAME =?"
				+ "AND YEAR(DUE_DATE)=? "
				+ "AND MONTH(DUE_DATE)=? "
				+ "AND AMOUNT>0";
	
		PreparedStatement sumStmt;
		try {
			sumStmt = userDAO.getJdbcConnection().prepareStatement(qry);
			sumStmt.setString(1, userId);
			sumStmt.setString(2, merchant);
			sumStmt.setString(3, dueYY);
			sumStmt.setString(4, dueMM);
			ResultSet rs = sumStmt.executeQuery();
			while (rs.next()) {
			sum = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			userDAO.dbDisconnect();
		}

		return sum;

	}

	@Override
	public List<Bill> retrieveBillbyDate(String userId, String dueYY, String dueMM) {
		List<Bill> byDateBillList  = new ArrayList<>();
		DAO userDAO = new DAO();
		userDAO.dbConnect();
		String qry = "SELECT * FROM BILL "
				+ "WHERE USER_ID=? AND "
				+ "YEAR(DUE_DATE)=? AND "
				+ "MONTH(DUE_DATE)=?"
				+ " ORDER BY DUE_DATE";
		PreparedStatement retrieveBillByDateStmt;

		try {
			retrieveBillByDateStmt = userDAO.getJdbcConnection().prepareStatement(qry);
			retrieveBillByDateStmt.setString(1, userId);
			retrieveBillByDateStmt.setString(2, dueYY);
			retrieveBillByDateStmt.setString(3, dueMM);
			ResultSet rs = retrieveBillByDateStmt.executeQuery();

			while (rs.next()) {
				String id = rs.getString("BILL_ID");
				int billId = Integer.parseInt(id);
				String refNum = rs.getString("REF_NUMBER");
				String merchantName = rs.getString("MERCHANT_NAME");
				String billAmount = rs.getString("AMOUNT");
				String billDue = rs.getString("DUE_DATE");
				Bill billList = new Bill(billId, refNum, merchantName, billAmount, billDue);
				byDateBillList.add(billList);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		userDAO.dbDisconnect();
		return byDateBillList;

	}

	@Override
	public List<Bill> retrieveBillByMerchant(String userId, String merchant, String dueYY, String dueMM) {
		List<Bill> byDateBillList  = new ArrayList<>();
		DAO userDAO = new DAO();
		userDAO.dbConnect();
		String qry = "SELECT * FROM BILL "
				+ "WHERE USER_ID=? AND "
				+ "MERCHANT_NAME=? AND "
				+ "YEAR(DUE_DATE)=? AND "
				+ "MONTH(DUE_DATE)=? "
				+ "ORDER BY DUE_DATE";
		PreparedStatement retrieveBillByMerchantStmt;
		try {
			retrieveBillByMerchantStmt = userDAO.getJdbcConnection().prepareStatement(qry);
			retrieveBillByMerchantStmt.setString(1, userId);
			retrieveBillByMerchantStmt.setString(2, merchant);
			retrieveBillByMerchantStmt.setString(3, dueYY);
			retrieveBillByMerchantStmt.setString(4, dueMM);
			ResultSet rs = retrieveBillByMerchantStmt.executeQuery();

			while (rs.next()) {
				String id = rs.getString("BILL_ID");
				int billId = Integer.parseInt(id);
				String refNum = rs.getString("REF_NUMBER");
				String merchantName = rs.getString("MERCHANT_NAME");
				String billAmount = rs.getString("AMOUNT");
				String billDue = rs.getString("DUE_DATE");
				Bill billList = new Bill(billId , refNum, merchantName, billAmount, billDue);
 
				byDateBillList.add(billList);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		userDAO.dbDisconnect();
		return byDateBillList;
	}
}
