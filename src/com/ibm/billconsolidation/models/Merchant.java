package com.ibm.billconsolidation.models;

public class Merchant {
	
	private String merchantId;
	private String merchantName;
	private String merchantType;
	
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getMerchantType() {
		return merchantType;
	}
	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public Merchant() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Merchant(String merchantId, String merchantName, String merchantType) {
		super();
		this.merchantId = merchantId;
		this.merchantName = merchantName;
		this.merchantType = merchantType;
	}
	@Override
	public String toString() {
		return "Merchant [merchantId=" + merchantId + ", merchantName=" + merchantName + ", merchantType="
				+ merchantType + "]";
	}
	public Merchant(String merchantName, String merchantType) {
		super();
		this.merchantName = merchantName;
		this.merchantType = merchantType;
	}

	

}
