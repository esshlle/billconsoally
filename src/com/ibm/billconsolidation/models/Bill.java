package com.ibm.billconsolidation.models;

import java.sql.Date;

public class Bill {
	private	int billId;
	private String userId;
	private String billRefNumber;
	private String merchantName;
	private String billPrice;
	private	String billDate;
	private String billDue;
	public Bill(String userId,String billRefNumber, String merchantName, String billPrice, String billDate, String billDue) {
		super();
		this.userId = userId;
		this.billRefNumber = billRefNumber;
		this.merchantName = merchantName;
		this.billPrice = billPrice;
		this.billDate = billDate;
		this.billDue = billDue;
	}
	public String getBillRefNumber() {
		return billRefNumber;
	}
	public void setBillRefNumber(String billRefNumber) {
		this.billRefNumber = billRefNumber;
	}
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	public String getBillPrice() {
		return billPrice;
	}
	public void setBillPrice(String billPrice) {
		this.billPrice = billPrice;
	}
	public String getBillDate() {
		return billDate;
	}
	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}
	public String getBillDue() {
		return billDue;
	}
	public void setBillDue(String billDue) {
		this.billDue = billDue;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Bill(int billId,String billRefNumber, String merchantName, String billPrice, String billDue) {
		super();
		this.billId = billId;
		this.billRefNumber = billRefNumber;
		this.merchantName = merchantName;
		this.billPrice = billPrice;
		this.billDue = billDue;
	}
	public Bill() {
		super();

	}
	public int getBillId() {
		return billId;
	}
	public void setBillId(int billId) {
		this.billId = billId;
	}
	
	

	
}