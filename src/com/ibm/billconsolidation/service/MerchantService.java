package com.ibm.billconsolidation.service;

import java.util.List;


import com.ibm.billconsolidation.models.Merchant;


public interface MerchantService {

	public List<Merchant> userMerchant(String userId);
	public List<Merchant> allMerchant();
	public boolean removeUserMerchant(String userId,String merchantId);
	public String addUserMerchant(String userId,String merchantId);
	public boolean deleteMerchant(String merchantId);
	public String addMerchant(Merchant newMerchant);

}
