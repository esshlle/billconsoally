package com.ibm.billconsolidation.service;



import com.ibm.billconsolidation.models.User;

public interface UserService {
	
	public String signUpUser(User newUser);

}
