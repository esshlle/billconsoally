package com.ibm.billconsolidation.service;


import java.util.List;

import com.ibm.billconsolidation.models.Bill;

public interface BillService {

	
	public List<Bill> retrieveAllUserBills(String userId);
	public List<Bill> retrieveBillbyDate(String userId, String dueYY, String dueMM);
	public List<Bill> retrieveBillByMerchant(String userId, String merchantId, String dueYY, String dueMM);
	public String addBill(Bill newBillInfo);
	public boolean removeBill(String billRefNumber);
	public String computeBill(String userId,  String dueYY, String dueMM);
	public String computeBillMerchant(String userId, String merchantId, String dueYY, String dueMM);

}
